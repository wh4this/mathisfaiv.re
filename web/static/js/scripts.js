var amountScrolled = 0,
    isBlurred = false;
    bgblurred = document.getElementById('background-image-blurred'),
    arrow = document.getElementById('arrow'),
    switchbutton = document.getElementById('switch-background-button'),
    hgroup = document.getElementById('hgroup'),
    previousIndex = 0,
    width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

if (width > 1300) {
    var images = ['images/1080/back1.jpg', 'images/1080/back2.jpg', 'images/1080/back3.jpg', 'images/1080/back4.jpg', 'images/1080/back5.jpg', 'images/1080/back6.jpg', 'images/1080/back7.jpg', 'images/1080/back8.jpg', 'images/1080/back9.jpg', 'images/1080/back10.jpg', 'images/1080/back11.jpg', 'images/1080/back12.jpg', 'images/1080/back13.jpg', 'images/1080/back14.jpg', 'images/1080/back15.jpg', 'images/1080/back16.jpg', 'images/1080/back17.jpg'];
    var imagesBlur = ['images/1080/back1-blur.jpg', 'images/1080/back2-blur.jpg', 'images/1080/back3-blur.jpg', 'images/1080/back4-blur.jpg', 'images/1080/back5-blur.jpg', 'images/1080/back6-blur.jpg', 'images/1080/back7-blur.jpg', 'images/1080/back8-blur.jpg', 'images/1080/back9-blur.jpg', 'images/1080/back10-blur.jpg', 'images/1080/back11-blur.jpg', 'images/1080/back12-blur.jpg', 'images/1080/back13-blur.jpg', 'images/1080/back14-blur.jpg', 'images/1080/back15-blur.jpg', 'images/1080/back16-blur.jpg', 'images/1080/back17-blur.jpg'];
} else {
    var images = ['images/back1.jpg', 'images/back2.jpg', 'images/back3.jpg', 'images/back4.jpg', 'images/back5.jpg', 'images/back6.jpg', 'images/back7.jpg', 'images/back8.jpg', 'images/back9.jpg', 'images/back10.jpg', 'images/back11.jpg', 'images/back12.jpg', 'images/back13.jpg', 'images/back14.jpg', 'images/back15.jpg', 'images/back16.jpg', 'images/back17.jpg'];
    var imagesBlur = ['images/back1-blur.jpg', 'images/back2-blur.jpg', 'images/back3-blur.jpg', 'images/back4-blur.jpg', 'images/back5-blur.jpg', 'images/back6-blur.jpg', 'images/back7-blur.jpg', 'images/back8-blur.jpg', 'images/back9-blur.jpg', 'images/back10-blur.jpg', 'images/back11-blur.jpg', 'images/back12-blur.jpg', 'images/back13-blur.jpg', 'images/back14-blur.jpg', 'images/back15-blur.jpg', 'images/back16-blur.jpg', 'images/back17-blur.jpg'];
}
var randIndex = Math.floor(Math.random() * images.length);

function removeActive() {
    document.getElementById('tab-item-about').classList.remove('active');
    document.getElementById('tab-item-resume').classList.remove('active');
    document.getElementById('tab-item-contact').classList.remove('active');
    document.getElementById('about-section').classList.remove('active');
    document.getElementById('resume-section').classList.remove('active');
    document.getElementById('contact-section').classList.remove('active');
}

function getAnchor() {
    var currentUrl = document.URL,
        urlParts = currentUrl.split('#');
    return (urlParts.length > 1) ? urlParts[1] : null;
}

function throttle(fn, wait) {
    var time = Date.now();
    return function() {
        if ((time + wait - Date.now()) < 0) {
            fn();
            time = Date.now();
        }
    }
}

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this,
            args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};
 
function getAmountScrolled() {
    amountScrolled = window.pageYOffset;
}

function toggleArrowSwitchButton(show) {
    if (show) {
        setTimeout(function() {
            arrow.classList.remove("hide");
            switchbutton.classList.remove("hide");
        }, 1)
    } else {
        setTimeout(function() {
            switchbutton.classList.add("hide");
            arrow.classList.add("hide");
        }, 1)
    }
}

function parallaxBlur() {
    throttle(getAmountScrolled(), 10);
    if (amountScrolled < 300) {
        if (amountScrolled >= 250) {
            bgblurred.style.opacity = 1;
            toggleArrowSwitchButton(false);
            isBlurred = true;
        } else if (amountScrolled < 100) {
            bgblurred.style.opacity = 0;
            toggleArrowSwitchButton(true);
            isBlurred = false;
        }
    } else { 
        if (!isBlurred) {
            bgblurred.style.opacity = 1;
            toggleArrowSwitchButton(false);
            isBlurred = true;
        }
    }
}

function switchBackground() {
    randIndex = Math.floor(Math.random() * images.length);
    while (randIndex == previousIndex) {
        randIndex = Math.floor(Math.random() * images.length);
    }
    previousIndex = randIndex;
    if (Math.floor(Math.random() * 100) === 99) {
        document.getElementById('background-image').style.background = 'url(images/coucou.jpg) no-repeat center center scroll';
        document.getElementById('background-image-blurred').style.background = 'url(images/coucou-blur.jpg) no-repeat center center scroll';
        document.getElementById('background-image-blurred-bottom').style.background = 'url(images/coucou-blur.jpg) no-repeat center center scroll';
        document.getElementById('hgroup').style.background = 'url(images/coucou-blur.jpg) no-repeat center center scroll';
    } else {
        document.getElementById('background-image').style.background = 'url(' + images[randIndex] + ') no-repeat center center scroll';
        document.getElementById('background-image-blurred').style.background = 'url(' + imagesBlur[randIndex] + ') no-repeat center center scroll';
        document.getElementById('background-image-blurred-bottom').style.background = 'url(' + imagesBlur[randIndex] + ') no-repeat center center scroll';
        document.getElementById('hgroup').style.background = 'url(' + imagesBlur[randIndex] + ') no-repeat center center scroll';
    }
}

// Event listeners
window.addEventListener("scroll", debounce(parallaxBlur, 10));
switchbutton.addEventListener("click", function() { switchBackground() });

// Opens the right tab when loading the page
if (getAnchor() === "contact") {
    removeActive();
    document.getElementById('tab-item-contact').classList.add('active');
    document.getElementById('contact-section').classList.add('active');
} else if (getAnchor() === "resume") {
    removeActive();
    document.getElementById('tab-item-resume').classList.add('active');
    document.getElementById('resume-section').classList.add('active');
}

// Background image on first load
document.getElementById('background-image').style.background = 'url(' + images[randIndex] + ') no-repeat center center scroll';
document.getElementById('background-image-blurred').style.background = 'url(' + imagesBlur[randIndex] + ') no-repeat center center scroll';
document.getElementById('background-image-blurred-bottom').style.background = 'url(' + imagesBlur[randIndex] + ') no-repeat center center scroll';
document.getElementById('hgroup').style.background = 'url(' + imagesBlur[randIndex] + ') no-repeat center center scroll';

// Tab navigation
document.getElementById('tab-item-about').addEventListener("click", function() {
    removeActive();
    document.getElementById('tab-item-about').classList.add('active');
    document.getElementById('about-section').classList.add('active');
});
document.getElementById('tab-item-resume').addEventListener("click", function() {
    removeActive();
    document.getElementById('tab-item-resume').classList.add('active');
    document.getElementById('resume-section').classList.add('active');
});
document.getElementById('tab-item-contact').addEventListener("click", function() {
    removeActive();
    document.getElementById('tab-item-contact').classList.add('active');
    document.getElementById('contact-section').classList.add('active');
});

// Form validation 
var nameForm = document.getElementById('name-email-form');
var emailForm = document.getElementById('address-email-form');
var messageForm = document.getElementById('message-email-form');
var formElement = document.getElementById('form-email');

var regexEmail = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g;

function check(element, event) {
    if (element.value == "") {
        if (event) {
            event.preventDefault();
        }
        element.classList.remove('is-success');
        element.classList.add('is-error');
    } else {
        element.classList.add('is-success');
        element.classList.remove('is-error');
    }
}

function checkEmail(element, event) {
    if (element.value == "" && !regexEmail.test(element.value)) {
        if (event) {
            event.preventDefault();
        }
        element.classList.remove('is-success');
        element.classList.add('is-error');
    } else {
        element.classList.add('is-success');
        element.classList.remove('is-error');
    }
}

nameForm.addEventListener("keyup", function() {
    check(nameForm, null);
})

emailForm.addEventListener("keyup", function() {
    checkEmail(emailForm, null);
})

messageForm.addEventListener("keyup", function() {
    check(messageForm, null);
})

formElement.addEventListener("submit", function(event) {
    check(nameForm, event);
    checkEmail(emailForm, event);
    check(messageForm, event);
});