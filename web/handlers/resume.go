package handlers

import (
	"bytes"
	"gitlab.com/mathisf/mathisfaiv.re/internal"
	"html/template"
	"log"
	"net/http"
)

func ResumeHandler(w http.ResponseWriter, r *http.Request, tmpl *template.Template) {
	internal.RedirectIfNoLang(w, r)

	lang := internal.DetectLanguage(r)
	cacheKey := "resume_" + lang

	cachedOutput, cached := internal.SharedCache().Get(cacheKey)
	if cached {
		_, err := w.Write(cachedOutput.([]byte))
		if err != nil {
			log.Printf("Error writing response: %v", err)
		}
		return
	}

	pageData, err := internal.LoadPageData(lang)
	if err != nil {
		log.Printf("failed to load page data: %v", err)
		http.Error(w, "Failed to load page data", http.StatusInternalServerError)
		return
	}

	var buf bytes.Buffer
	if err := tmpl.ExecuteTemplate(&buf, "main.gohtml", pageData); err != nil {
		log.Printf("Error rendering resume template: %v", err)
		http.Error(w, "Error rendering resume template", http.StatusInternalServerError)
		return
	}

	internal.SharedCache().Set(cacheKey, buf.Bytes())

	_, err = w.Write(buf.Bytes())
	if err != nil {
		log.Printf("Error writing response: %v", err)
	}
}
