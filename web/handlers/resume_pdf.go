package handlers

import (
	"bytes"
	"fmt"
	"gitlab.com/mathisf/mathisfaiv.re/internal"
	"log"
	"mime/multipart"
	"net/http"
	"os"
)

func ResumePDFHandler(w http.ResponseWriter, r *http.Request) {
	internal.RedirectIfNoLang(w, r)

	lang := internal.DetectLanguage(r)
	cacheKey := "resume_pdf_" + lang

	if cachedPDF, found := internal.SharedCache().Get(cacheKey); found {
		fmt.Fprintln(os.Stdout, "Serving PDF from cache for language:", lang)
		servePDFBytes(w, cachedPDF.([]byte))
		return
	}

	gotenbergURL := "http://gotenberg:3000/forms/chromium/convert/url"
	htmlURL := fmt.Sprintf("http://go:8080/%s/resume.html", lang)
	var requestBody bytes.Buffer
	writer := multipart.NewWriter(&requestBody)

	if err := writer.WriteField("url", htmlURL); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := writer.WriteField("waitDelay", "3s"); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	writer.Close()

	req, err := http.NewRequest("POST", gotenbergURL, &requestBody)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	var pdfBuffer bytes.Buffer
	_, err = pdfBuffer.ReadFrom(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	internal.SharedCache().Set(cacheKey, pdfBuffer.Bytes())

	servePDFBytes(w, pdfBuffer.Bytes())
}

func servePDFBytes(w http.ResponseWriter, pdfData []byte) {
	w.Header().Set("Content-Type", "application/pdf")
	w.Header().Set("Content-Disposition", "attachment; filename=resume.pdf")
	_, err := w.Write(pdfData)
	if err != nil {
		log.Printf("Error writing response: %v", err)
	}
}
