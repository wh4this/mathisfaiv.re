package main

import (
	"fmt"
	"gitlab.com/mathisf/mathisfaiv.re/internal"
	"gitlab.com/mathisf/mathisfaiv.re/web"
	"log"
	"net/http"
)

func newServer() *http.ServeMux {
	indexTmpl, err := internal.GetIndexTemplate()
	if err != nil {
		fmt.Println("Error loading index template:", err)
		return nil
	}

	resumeTmpl, err := internal.GetResumeTemplate()
	if err != nil {
		fmt.Println("Error loading resume template:", err)
		return nil
	}

	return web.InitRouter(indexTmpl, resumeTmpl)
}

func main() {
	srv := &http.Server{
		Addr:    ":8080",
		Handler: newServer(),
	}
	log.Println("Starting server on :8080")
	log.Fatal(srv.ListenAndServe())
}
