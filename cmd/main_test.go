// ./cmd/main_test.go
package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRoutes(t *testing.T) {
	tests := []struct {
		name       string
		method     string
		url        string
		wantStatus int
		wantBody   *string
	}{
		{
			name:       "Root route",
			method:     http.MethodGet,
			url:        "/",
			wantStatus: http.StatusFound,
		},
		{
			name:       "Index route (with html extension)",
			method:     http.MethodGet,
			url:        "/fr/index.html",
			wantStatus: http.StatusOK,
		},
		{
			name:       "Resume route",
			method:     http.MethodGet,
			url:        "/en/resume",
			wantStatus: http.StatusOK,
		},
		{
			name:       "Resume route (with html extension)",
			method:     http.MethodGet,
			url:        "/fr/resume.html",
			wantStatus: http.StatusOK,
		},
		{
			name:       "Asset route",
			method:     http.MethodGet,
			url:        "/images/back1.jpg",
			wantStatus: http.StatusOK,
		},

		// {
		//     name:       "Resume PDF route",
		//     method:     http.MethodGet,
		//     url:        "/resume.pdf",
		//     wantStatus: http.StatusOK,
		// },
	}

	handler := newServer()

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req := httptest.NewRequest(tt.method, tt.url, nil)
			rr := httptest.NewRecorder()

			handler.ServeHTTP(rr, req)

			if rr.Code != tt.wantStatus {
				t.Errorf("expected status %d, got %d", tt.wantStatus, rr.Code)
			}
		})
	}
}
