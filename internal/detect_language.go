package internal

import (
	"net/http"
	"strings"
)

func DetectLanguage(r *http.Request) string {
	path := r.URL.Path
	if path == "/fr" || strings.HasPrefix(path, "/fr/") {
		return "fr"
	} else if path == "/en" || strings.HasPrefix(path, "/en/") {
		return "en"
	}

	if strings.HasPrefix(r.Header.Get("Accept-Language"), "fr") {
		return "fr"
	}
	return "en"
}

func RedirectIfNoLang(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	if path == "/fr" || path == "/en" {
		http.Redirect(w, r, path+"/", http.StatusFound)
		return
	}
	if strings.HasPrefix(path, "/fr/") || strings.HasPrefix(path, "/en/") {
		return
	}

	lang := DetectLanguage(r)
	http.Redirect(w, r, "/"+lang+path, http.StatusFound)
}
