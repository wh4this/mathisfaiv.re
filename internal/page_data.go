package internal

import (
	"embed"
	"encoding/json"
	"fmt"
)

//go:embed page_data/*
var pageDataFS embed.FS

func LoadPageData(lang string) (PageData, error) {
	filename := fmt.Sprintf("page_data/%s.json", lang)
	data, err := pageDataFS.ReadFile(filename)
	if err != nil {
		return PageData{}, err
	}

	var pageData PageData
	if err := json.Unmarshal(data, &pageData); err != nil {
		return PageData{}, err
	}
	return pageData, nil
}
