<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
      integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
      crossorigin="anonymous"
      referrerpolicy="no-referrer"
    />

    <title>{{ .Meta.Title }}</title>
    <style>
{{template "styles.css" . }}
       </style>
  </head>
  <body>
    <header id="header" class="clear">
      <img
        class="image"
        src="{{ .PersonalInfos.Image }}"
        alt="{{ .PersonalInfos.Name }}"
      />
      <div class="middle">
        <h1 class="name">{{ .PersonalInfos.Name }}</h1>
        <h2 class="label">{{ .PersonalInfos.Label }}</h2>
      </div>

      <span class="location">
        {{ if .PersonalInfos.Location.City }}
        <span class="city">{{ .PersonalInfos.Location.City }},</span>
        {{ end }} {{ if .PersonalInfos.Location.Region }}
        <span class="region">{{ .PersonalInfos.Location.Region }}</span>
        {{ end }} {{ if .PersonalInfos.Location.CountryCode }}
        <span class="countryCode"
          >{{ .PersonalInfos.Location.CountryCode }}</span
        >
        {{ end }}
      </span>

      <div id="contact">
        {{ if .PersonalInfos.URL }}
        <div class="website">
          <span class="fa-solid fa-up-right-from-square"></span>
          <a
            class="hide-href-print"
            target="_blank"
            target="_blank"
            href="{{ .PersonalInfos.URL }}"
            >{{ .PersonalInfos.URL }}</a
          >
        </div>
        {{ end }} {{ if .PersonalInfos.Email }}
        <div class="email">
          <span class="fa-regular fa-envelope"></span>
          <a class="hide-href-print" href="mailto:{{ .PersonalInfos.Email }}"
            >{{ .PersonalInfos.Email }}</a
          >
        </div>
        {{ end }} {{ if .PersonalInfos.Phone }}
        <div class="phone">
          <span class="fa-solid fa-mobile-screen-button"></span>
          <a class="hide-href-print" href="tel:{{ .PersonalInfos.Phone }}"
            >{{ .PersonalInfos.Phone }}</a
          >
        </div>
        {{ end }}
      </div>

      {{ if .PersonalInfos.Profiles }}
      <div id="profiles">
        {{ range $index, $profile := .PersonalInfos.Profiles }}
        <div class="item">
          {{ if $profile.Network }}
          <div class="username">
            <span
              class="fa-brands fa-{{ $profile.Network }} {{ $profile.Network }} social"
            ></span>
            {{ if $profile.URL }}
            <span class="url">
              <a target="_blank" href="{{ $profile.URL }}"
                >{{ $profile.Username }}</a
              >
            </span>
            {{else}}
            <span>{{ $profile.Username }}</span>
            {{ end }}
          </div>
          {{ end }}
        </div>
        {{ end }}
      </div>
      {{ end }}
    </header>

    {{ if .PersonalInfos.Summary }}
    <section class="section">
      <section class="main-summary">
        <div>{{ .PersonalInfos.Summary }}</div>
      </section>
    </section>
    {{ end }}

    <section class="section">
      <header>
        <h2 class="section-title">
          {{ .Sections.Resume.EmploymentHistory.Title | htmlSafe }}
        </h2>
      </header>
      <section id="work">
        {{range $index, $position :=
        .Sections.Resume.EmploymentHistory.Positions}}
        <section class="work-item">
          <header class="clear">
            <div class="date">{{ $position.YearRange}}</div>
            <div class="position">{{ $position.Title }}</div>
            <div class="company">{{ $position.Company }}</div>
          </header>

          {{ if $position.Location }}
          <span class="location">
            <span class="fa-solid fa-location-dot"></span>
            <span class="city">
              {{ if $position.Location.City }}
              <span class="city">{{ $position.Location.City }},</span>
              {{ end }} {{ if $position.Location.Region }}
              <span class="region">{{ $position.Location.Region }}</span>
              {{ end }} {{ if $position.Location.CountryCode }}
              <span class="countryCode"
                >{{ $position.Location.CountryCode }}</span
              >
              {{ end }}
            </span>
          </span>
          {{ end }}

          <div class="item" id="work-item">{{ $position.Summary}}</div>
          <div class="item" id="work-item">
            <ul>
              <li>
                {{ $.Sections.Resume.Languages | htmlSafe }}:&nbsp;{{- range $i, $thing := $position.Languages -}}
                {{ $thing | htmlSafe }} {{- if lt (add $i 1) (len
                $position.Languages) -}} ,&nbsp; {{- end }} {{- end }}
              </li>
              <li>
                {{ $.Sections.Resume.Frameworks | htmlSafe }}:&nbsp;{{- range $i, $thing := $position.Frameworks
                -}} {{ $thing | htmlSafe }} {{- if lt (add $i 1) (len
                $position.Frameworks) -}} ,&nbsp; {{- end }} {{- end }}
              </li>
              <li>
                {{ $.Sections.Resume.Sectors | htmlSafe }}:&nbsp;{{- range $i, $thing := $position.Sectors -}} {{
                $thing | htmlSafe }} {{- if lt (add $i 1) (len
                $position.Sectors) -}} ,&nbsp; {{- end }} {{- end }}
              </li>
            </ul>
            <br />
          </div>
        </section>
        {{ end }}
      </section>

      <section class="section">
        <header>
          <h2 class="section-title">
            {{ .Sections.Resume.Education.Title | htmlSafe }}
          </h2>
        </header>

        <section id="education">
          {{ range $index, $education := .Sections.Resume.Education.Degrees }}
          <section class="education-item">
            <header class="clear">
              <div class="date">
                <span class="startDate"> {{ $education.Year }} </span>
              </div>
              <div class="header-left">
                <div class="studyType">{{ $education.Title | htmlSafe }}</div>
                <div class="institution">{{ $education.School }}</div>
              </div>
            </header>
          </section>
          {{ end }}
        </section>
      </section>
      <section class="section">
        <header>
          <h2 class="section-title">
            {{ .Sections.Resume.PersonalProjects.Title | htmlSafe }}
          </h2>
        </header>
        <section id="projects">
          {{ range $index, $project := .Sections.Resume.PersonalProjects.List }}
          <section class="project-item">
            {{ if $project.Description }}
            <label for="project-item-{{ $index }}"></label>
            {{ end }} {{ if $project.Name }}
            <header class="clear">
              {{ if $project.Name }}
              <div class="position">{{ $project.Name | htmlSafe }}</div>
              {{ end }}
            </header>
            {{ end }} {{ if $project.Link }}
            <span class="website">
              <span class="fa-solid fa-up-right-from-square"></span>
              <a target="_blank" href="{{ $project.Link }}"
                >{{ $project.Name }}</a
              >
            </span>
            {{ end }}
            <div class="item">
              {{ if $project.Description }}
              <div class="summary">{{ $project.Description | htmlSafe }}</div>
              {{ end }} {{ if $project.Tech }}
              <p>{{ $project.Tech }}</p>
              {{ end }}
            </div>
          </section>
          {{ end }}
        </section>
      </section>
      <section class="section">
        <header>
          <h2 class="section-title">{{ .PersonalInfos.Languages.Title }}</h2>
        </header>
        <section id="languages">
          {{ range $language := .PersonalInfos.Languages.List }}
          <div class="display">
            <h3 class="language">{{ $language.Title }}</h3>
            <div class="item">
              {{ if $language.Level }}
              <em>{{ $language.Level }}</em>
              {{ end }}
            </div>
          </div>
          {{ end }}
        </section>
      </section>
      <section class="section">
        <header>
          <h2 class="section-title">
            {{ .Sections.Resume.PersonalInterests.Title | htmlSafe }}
          </h2>
        </header>
        <section id="interests">
          {{ $interests := .Sections.Resume.PersonalInterests }}
          <div class="item">
            {{ if $interests.Music.Title }}
            <h3 class="name">{{ $interests.Music.Title }}</h3>
            {{ if $interests.Music.List }}
            <ul class="keywords">
              {{ range $keyword := $interests.Music.List }}
              <li>{{ $keyword }}</li>
              {{ end }}
            </ul>
            {{ end }} {{ end }}
          </div>

          <div class="item">
            {{ if $interests.Cinema.Title }}
            <h3 class="name">{{ $interests.Cinema.Title }}</h3>
            {{ if $interests.Cinema.List }}
            <ul class="keywords">
              {{ range $keyword := $interests.Cinema.List }}
              <li>{{ $keyword }}</li>
              {{ end }}
            </ul>
            {{ end }} {{ end }}
          </div>

          <div class="item">
            {{ if $interests.Literature.Title }}
            <h3 class="name">{{ $interests.Literature.Title }}</h3>
            {{ if $interests.Literature.List }}
            <ul class="keywords">
              {{ range $keyword := $interests.Literature.List }}
              <li>{{ $keyword }}</li>
              {{ end }}
            </ul>
            {{ end }} {{ end }}
          </div>

          <div class="item">
            {{ if $interests.VideoGames.Title }}
            <h3 class="name">{{ $interests.VideoGames.Title }}</h3>
            {{ if $interests.VideoGames.List }}
            <ul class="keywords">
              {{ range $keyword := $interests.VideoGames.List }}
              <li>{{ $keyword }}</li>
              {{ end }}
            </ul>
            {{ end }} {{ end }}
          </div>

          <div class="item">
            {{ if $interests.Homelab.Title }}
            <h3 class="name">{{ $interests.Homelab.Title }}</h3>
            {{ if $interests.Homelab.List }}
            <ul class="keywords">
              {{ range $keyword := $interests.Homelab.List }}
              <li>{{ $keyword }}</li>
              {{ end }}
            </ul>
            {{ end }} {{ end }}
          </div>
        </section>
      </section>
    </section>
  </body>
</html>
