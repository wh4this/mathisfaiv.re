package internal

type Meta struct {
	Title       string   `json:"title"`
	Description string   `json:"description"`
	Author      string   `json:"author"`
	Keywords    []string `json:"keywords"`
	Language    string   `json:"language"`
}

type About struct {
	Title    string `json:"title"`
	Content1 string `json:"content_1"`
	Content2 string `json:"content_2"`
	Content3 string `json:"content_3"`
}

type EmploymentHistory struct {
	Title         string           `json:"title"`
	Intro         string           `json:"intro"`
	TimelineIntro string           `json:"timeline_intro"`
	Positions     []ResumePosition `json:"positions"`
}

type EducationDegree struct {
	Year       string `json:"year"`
	Title      string `json:"title"`
	WithHonors string `json:"with_honors"`
	School     string `json:"school"`
}

type Education struct {
	Title   string            `json:"title"`
	Degrees []EducationDegree `json:"degrees"`
}

type Languages struct {
	Title string `json:"title"`
	List  []struct {
		Title string `json:"title"`
		Level string `json:"level"`
	} `json:"list"`
}

type SkillSection struct {
	Title string `json:"title"`
	List  []struct {
		Title string   `json:"title"`
		List  []string `json:"list"`
	} `json:"list"`
}

type Skills struct {
	Title string         `json:"title"`
	List  []SkillSection `json:"list"`
}

type PersonalProject struct {
	Name        string `json:"name"`
	Tech        string `json:"tech"`
	Description string `json:"description"`
	Git         string `json:"git"`
	Link        string `json:"link"`
}

type PersonalProjects struct {
	Title string            `json:"title"`
	Visit string            `json:"visit"`
	List  []PersonalProject `json:"list"`
}

type PersonalInterests struct {
	Title string `json:"title"`
	Music struct {
		Title string   `json:"title"`
		List  []string `json:"list"`
	} `json:"music"`
	Cinema struct {
		Title string   `json:"title"`
		List  []string `json:"list"`
	} `json:"cinema"`
	Literature struct {
		Title string   `json:"title"`
		List  []string `json:"list"`
	} `json:"literature"`
	VideoGames struct {
		Title string   `json:"title"`
		List  []string `json:"list"`
	} `json:"video_games"`
	Homelab struct {
		Title string   `json:"title"`
		List  []string `json:"list"`
	} `json:"homelab"`
}

type Contact struct {
	Title     string `json:"title"`
	Links     string `json:"links"`
	EmailForm struct {
		Title string `json:"title"`
		Name  struct {
			Label       string `json:"label"`
			Placeholder string `json:"placeholder"`
		} `json:"name"`
		Email struct {
			Label       string `json:"label"`
			Placeholder string `json:"placeholder"`
		} `json:"email"`
		Message struct {
			Label       string `json:"label"`
			Placeholder string `json:"placeholder"`
		} `json:"message"`
		Submit string `json:"submit"`
	} `json:"email_form"`
	EmailButton    string `json:"email_button"`
	GitlabButton   string `json:"gitlab_button"`
	LinkedinButton string `json:"linkedin_button"`
}

type Footer struct {
	BackToTopButton string `json:"back_to_top_button"`
	Made            string `json:"made"`
	Copyrights      string `json:"copyrights"`
	Licence         string `json:"licence"`
}

type ResumePosition struct {
	YearRange        string   `json:"year_range"`
	YearCount        int      `json:"year_count"`
	Company          string   `json:"company"`
	CompanyLink      string   `json:"company_link"`
	Title            string   `json:"title"`
	ContractType     string   `json:"contract_type"`
	Responsibilities []string `json:"responsibilities"`
	Sectors          []string `json:"sectors"`
	Languages        []string `json:"languages"`
	Frameworks       []string `json:"frameworks"`
	Location         Location `json:"location"`
	Summary          string   `json:"summary"`
}

type Resume struct {
	Title             string            `json:"title"`
	DownloadFrench    string            `json:"download_french"`
	DownloadEnglish   string            `json:"download_english"`
	Download          string            `json:"download"`
	Sectors           string            `json:"sectors"`
	Languages         string            `json:"languages"`
	Frameworks        string            `json:"frameworks"`
	EmploymentHistory EmploymentHistory `json:"employment_history"`
	Education         Education         `json:"education"`
	Skills            Skills            `json:"skills"`
	PersonalProjects  PersonalProjects  `json:"personal_projects"`
	PersonalInterests PersonalInterests `json:"personal_interests"`
}

type Sections struct {
	About   About   `json:"about"`
	Resume  Resume  `json:"resume"`
	Contact Contact `json:"contact"`
}

type PersonalInfos struct {
	Name      string    `json:"name"`
	Label     string    `json:"label"`
	Image     string    `json:"image"`
	Email     string    `json:"email"`
	Phone     string    `json:"phone"`
	Location  Location  `json:"location"`
	URL       string    `json:"url"`
	Profiles  []Profile `json:"profiles"`
	Languages Languages `json:"Languages"`
	Summary   string    `json:"summary"`
}

type Location struct {
	City        string `json:"city"`
	CountryCode string `json:"countryCode"`
	Region      string `json:"region"`
}

type Profile struct {
	Network  string `json:"network"`
	Username string `json:"username"`
	URL      string `json:"url"`
}

type PageData struct {
	Name                          string        `json:"name"`
	Title                         string        `json:"title"`
	ChangeBackgroundButtonTooltip string        `json:"change_background_button_tooltip"`
	Meta                          Meta          `json:"meta"`
	Sections                      Sections      `json:"sections"`
	Footer                        Footer        `json:"footer"`
	PersonalInfos                 PersonalInfos `json:"personal_infos"`
}
