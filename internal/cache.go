package internal

import (
	"sync"
)

type Cache struct {
	data map[string]interface{}
	lock sync.RWMutex
}

var sharedCache = &Cache{
	data: make(map[string]interface{}),
}

func (c *Cache) Get(key string) (interface{}, bool) {
	c.lock.RLock()
	defer c.lock.RUnlock()
	value, found := c.data[key]
	return value, found
}

func (c *Cache) Set(key string, value interface{}) {
	c.lock.Lock()
	defer c.lock.Unlock()
	c.data[key] = value
}

func SharedCache() *Cache {
	return sharedCache
}
