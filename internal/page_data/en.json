{
  "name": "Mathis F.",
  "title": "Lead Developer specialized in Web apps and DevOps",
  "change_background_button_tooltip": "Change background picture",
  "meta": {
    "title": "Mathis F. | Lead Developer",
    "description": "Mathis F. website, Mathis is a Lead Developer specialized in Web apps and DevOps from France.",
    "author": "Mathis F.",
    "keywords": [
      "Mathis",
      "Lead Developer",
      "Software Developer",
      "Resume",
      "Contact"
    ],
    "language": "en"
  },
  "personal_infos": {
    "name": "Mathis F.",
    "label": "Fullstack Lead Developer",
    "image": "images/profile.jpg",
    "email": "m@this.fr",
    "phone": "",
    "location": {
      "city": "Nantes",
      "countryCode": "FR",
      "region": "Pays de la Loire"
    },
    "url": "https://mathisfaiv.re",
    "summary": "I am a Lead Developer specialized in web application development with PHP, Symfony, API Platform, and Angular. I also have DevOps skills with Docker, Ansible, Compose, Gitlab CI/CD, and cloud infrastructure (particularly Azure).",
    "profiles": [
      {
        "network": "gitlab-square",
        "username": "mathisf",
        "url": "https://gitlab.com/mathisf"
      },
      {
        "network": "linkedin",
        "username": "mathisf",
        "url": "https://www.linkedin.com/in/mathis-f-6246b2146"
      },
      {
        "network": "square-github",
        "username": "mathisf",
        "url": "https://github.com/mathisf"
      }
    ],
    "languages": {
      "title": "Languages",
      "list": [
        { "title": "French", "level": "Mother tongue" },
        { "title": "English", "level": "Advanced level" }
      ]
    }
  },
  "sections": {
    "about": {
      "title": "About",
      "content_1": "Hello, I'm Mathis 👋 I'm a lead developer specialized in Web app development and DevOps practices, from France. I am passionate about new technologies, music, movies and books about nature. I really like making web applications or web sites because I feel like I can express myself better on the web. I like learning new things, especially from people who know it well.",
      "content_2": "I believe in creating solutions that are simple, performant, and standard-compliant.",
      "content_3": "If you want to know more about me, feel free to contact me at&nbsp;<a href=\"mailto:m@this.fr\">m@this.fr</a> or check the contact section of this website."
    },
    "resume": {
      "title": "Resume",
      "download": "Download PDF",
      "download_french": "French",
      "download_english": "English",
      "sectors": "Sectors",
      "languages": "Languages",
      "frameworks": "Frameworks",
      "employment_history": {
        "title": "Employment history",
        "intro": "I'm a software engineer specialized in web development in permanent contract since 2021. In 2022, I became a DevOps engineer in addition to my current role (via internal training and self-study). Finally, in 2023, I was promoted to Lead Software Developer.",
        "timeline_intro": "Here is the fully detailed timeline:",
        "positions": [
          {
            "year_range": "2023 - now",
            "year_count": 2,
            "company": "A5Sys",
            "company_link": "https://a5sys.com",
            "title": "Lead Software Developer / DevOps Engineer",
            "contract_type": "Permanent contract",
            "summary": "As a Lead Software Developer and DevOps Engineer, I lead the development of B2B and B2C web applications while driving architecture decisions and team management. I have introduced DevOps best practices such as GitOps and Docker across the company and led initiatives that improved CI/CD pipelines and infrastructure automation. Additionally, I provide technical training and guidance, helping colleagues and clients adopt new tools and workflows. My work also includes overseeing cost and time estimations, and a little mobile development on both iOS and Android",
            "location": {
              "city": "Nantes",
              "countryCode": "FR",
              "region": "Pays de la Loire"
            },
            "responsibilities": [
              "Carrying out similar duties as a developer, but with additional responsibilities and new assignments",
              "Made <b>architecture decisions</b> for multiple B2B and B2C web applications and was responsible for leading their development with a team of up to 5 people",
              "Trained colleagues and taught them our best practices in web application development",
              "Performed <b>cost and time estimates</b> for web development projects",
              "Presented the concept of <b>GitOps</b> to the company and also published an <a href=\"https://www.a5sys.com/gitops-explique-par-mathis/\">article about it (in french)</a>",
              "Contributed to the internal web application starter kit (a generic, reusable application to kickstart clients projects)",
              "Implemented a Go JSON translator, that uses GPT4o",
              "Enhanced self-hosted GitLab architecture : <ul><li>Added a dedicated GitLab-runner instance with Docker executor</li><li>Migrated jobs and pipelines to use Docker executor instead of the unsafe shell executor</li><li>Integrated an S3 cache server to cache specific tasks in jobs (e.g. dependencies fetching, package manager cache)</li></ul>",
              "Delivered multiple Docker <b>training sessions</b> to over <b>twenty colleagues and clients</b>, who now utilize Docker in production and are migrating their infrastructure",
              "Implemented a <b>code search server</b> based on Hound, enabling multi-project Git searches (equivalent to GitLab's premium feature)",
              "Deployed <b>iOS and Android</b> apps using TestFlight and Google Play Console",
              "Integrated <b>Ansible</b> into client projects and GitLab jobs/pipelines to maintain infrastructure as code, streamline deployment processes, and facilitate installation on new servers"
            ],
            "sectors": [
              "Finance: Debt recovery management",
              "Services: Meeting recording and note taking app",
              "Energy: CEE management, solar panels",
              "Industry: Garages, doors, car sales, specific car part sales",
              "Agriculture: Cow milk analysis, Cow sensors"
            ],
            "languages": [
              "PHP",
              "TypeScript",
              "SQL (Mostly PostgreSQL, but also MySQL and SQL Server)",
              "Yaml (Gitlab CI/CD, Ansible)",
              "POSIX Shell",
              "Golang"
            ],
            "frameworks": [
              "Symfony 3 to 7",
              "Angular 5 to 18",
              "Angular Material MDC",
              "Ansible",
              "API Platform 2 to 3"
            ]
          },
          {
            "year_range": "2019 – 2023",
            "company": "A5Sys",
            "company_link": "https://a5sys.com",
            "year_count": 3,
            "title": "Software developer / DevOps Engineer",
            "summary": "At A5Sys, I worked as a software developer and DevOps engineer, contributing to various projects ranging from web application development to CI/CD pipeline setups. I focused on integrating Docker, GitLab CI, and upgrading/migrating Symfony and Angular applications, while also providing technical guidance and training to colleagues.",
            "location": {
              "city": "Nantes",
              "countryCode": "FR",
              "region": "Pays de la Loire"
            },
            "contract_type": "As an intern for 5 months, as an apprentice for 1 year and then in a 2 years permanent contract",
            "responsibilities": [
              "Developed web applications using Symfony 3-6, API Platform 2-3, and Angular 5-15",
              "Worked with multiple developers, typically in small teams of up to 5 people",
              "Maintained and upgraded the company's GitLab server and runner",
              "Wrote specifications for new functionalities",
              "Upgraded web applications to the latest versions of Symfony/Angular",
              "Set up a custom Gitlab Runner (using Docker Executor)",
              "Set up Docker and GitLab CI pipelines on web applications (lint, deploy)",
              "Set up Mock-server for company-wide API mocking (applying simple GitOps principles)",
              "Worked on small proofs of concepts (JadX decompilation, PostGIS + Leaflet map)",
              "Trained colleagues in web application development",
              "Wrote and automated end-to-end tests using Cypress",
              "Wrote tests using PHPUnit"
            ],
            "sectors": [
              "Services: Meeting recording and note taking app",
              "Energy: CEE management, solar panels",
              "Industry: Car sales, software developers time management and planning",
              "Agriculture: Cow milk analysis"
            ],
            "languages": [
              "PHP",
              "Javascript",
              "TypeScript",
              "HTML",
              "CSS",
              "MySQL",
              "Microsoft SQL Server",
              "PostgreSQL",
              "Shell"
            ],
            "frameworks": [
              "Symfony 2 to 5",
              "Angular 5 to 15",
              "Angular Material",
              "API Platform 2 to 3"
            ]
          },
          {
            "year_range": "2017 – 2019",
            "company": "Habitat 44",
            "company_link": "https://habitat44.org",
            "contract_type": "As an intern for 8 months and 8 months part-time",
            "year_count": 1,
            "title": "Full-stack web developer",
            "location": {
              "city": "Nantes",
              "countryCode": "FR",
              "region": "Pays de la Loire"
            },
            "summary": "I contributed to the development of a full-stack web application from scratch, handling both frontend (Angular) and backend (Symfony) tasks. My role also involved deploying the application with scripts, setting up a new GitLab self-hosted instance. We used Scrum methodology.",
            "responsibilities": [
              "Worked on a web application from scratch, using Angular 6 (as front-end) and Symfony 3 (as a RESTful API)",
              "Deployed the application on production servers",
              "Installed and configured Gitlab (with Active Directory and SMTP)",
              "Joined a team of 4 developers",
              "Assisted in the implementation of Scrum"
            ],
            "languages": [
              "TypeScript",
              "Javascript",
              "PHP",
              "HTML",
              "CSS",
              "Shell"
            ],
            "frameworks": ["Symfony 3", "Angular 2-6"],
            "sectors": ["Social housing"]
          }
        ]
      },
      "education": {
        "title": "Education",
        "degrees": [
          {
            "year": "2020",
            "title": "<abbr title=\"Computer Science Technology applicable to the Management of Businesses\">MIAGE</abbr> Master's degree with honors",
            "with_honors": "with honors",
            "school": "Faculté des Sciences et des Techniques, Nantes (France)"
          },
          {
            "year": "2018",
            "title": "<abbr title=\"Computer Science Technology applicable to the Management of Businesses\">MIAGE</abbr> Licence degree",
            "with_honors": "",
            "school": "Faculté des Sciences et des Techniques, Nantes (France)"
          },
          {
            "year": "2017",
            "title": "DUT in computer science",
            "with_honors": "",
            "school": "IUT de Nantes, Nantes (France)"
          },
          {
            "year": "2015",
            "title": "Baccalauréat in Electronics with honors",
            "with_honors": "with honors",
            "school": "Lycée Jean De Lattre De Tassigny, La Roche Sur Yon (France)"
          }
        ]
      },
      "skills": {
        "title": "Skills",
        "list": [
          {
            "title": "IT Skills",
            "list": [
              {
                "title": "Languages",
                "list": [
                  "TypeScript",
                  "PHP",
                  "JavaScript",
                  "POSIX Shell",
                  "Elixir",
                  "Golang",
                  "Java",
                  "Python"
                ]
              },
              {
                "title": "Markup languages",
                "list": ["HTML", "CSS", "XML", "LaTeX"]
              },
              {
                "title": "Databases",
                "list": [
                  "PostgreSQL (and PostGIS extension)",
                  "MySQL/MariaDB",
                  "Microsoft SQL Server",
                  "Informix"
                ]
              },
              {
                "title": "Frameworks",
                "list": [
                  "Angular",
                  "Symfony",
                  "API Platform",
                  "Angular Material",
                  "PrimeNG",
                  "NextJS",
                  "Cypress",
                  "Elixir Phoenix",
                  "Django",
                  "Svelte"
                ]
              },
              {
                "title": "OS",
                "list": [
                  "Linux (debian-based, fedora, arch)",
                  "macOS",
                  "Windows"
                ]
              },
              {
                "title": "SaaS/PaaS/DBaaS",
                "list": [
                  "Azure (VM, Blob storage, CDN, AD B2C, Entra ID)",
                  "Cloudflare (DNS, Proxy, D2 Storage)",
                  "Vercel (static web sites with CD)",
                  "Supabase (Auth, blob storage)",
                  "Google AppEngine (Appengine, Bigtable)",
                  "Firebase (Realtime DB)",
                  "Heroku (Web apps)"
                ]
              },
              {
                "title": "Softwares",
                "list": [
                  "VS Code",
                  "Neovim",
                  "Jetbrain's IDEs",
                  "DBeaver",
                  "GitLab",
                  "Android Studio",
                  "XCode",
                  "Eclipse",
                  "Office Suite"
                ]
              },
              {
                "title": "Tools",
                "list": [
                  "git",
                  "docker",
                  "docker compose plugin",
                  "Ansible",
                  "wsl2",
                  "Systemd",
                  "Make",
                  "plantuml",
                  "mermaid",
                  "PHPUnit",
                  "JMeter",
                  "JadX",
                  "GDB"
                ]
              },
              {
                "title": "Notions",
                "list": [
                  "Algorithms",
                  "Networking",
                  "Assembly",
                  "UI/UX Design",
                  "Usability and accessibility",
                  "Computer architecture"
                ]
              },
              {
                "title": "Methodologies",
                "list": [
                  "Agile",
                  "Scrum",
                  "V model",
                  "Rapid Application Development (RAD)"
                ]
              }
            ]
          },
          {
            "title": "General skills",
            "list": [
              {
                "title": "Languages",
                "list": ["French (mother tongue)", "English (advanced level)"]
              },
              {
                "title": "Conception",
                "list": [
                  "UML",
                  "Design patterns (most used: DI, MVC, Factory, Decorator, <br>Repository, Adapter, Delegation, Singleton)"
                ]
              },
              {
                "title": "Notions",
                "list": ["Accounting", "Enterprise management and organization"]
              }
            ]
          },
          {
            "title": "Certificates",
            "list": [
              {
                "title": "Azure",
                "list": [
                  "AZ-204 Microsoft Certified: Azure Developer Associate (2022)"
                ]
              }
            ]
          }
        ]
      },
      "personal_projects": {
        "title": "Personal projects",
        "visit": "Visit",
        "list": [
          {
            "name": "whnex",
            "tech": "Elixir and Phoenix Framework Web app",
            "description": "<a href=\"https://whnex.com/\">Unofficial Hacker News client</a>",
            "git": "https://gitlab.com/mathisf/whnex",
            "link": "https://whnex.com"
          },
          {
            "name": "AngieRecipes",
            "tech": "Angular and Symfony Web app",
            "description": "Your online personal recipe book",
            "git": "https://gitlab.com/mathisf/angierecipes",
            "link": "https://angierecipes.app"
          },
          {
            "name": "mathisfaiv.re",
            "tech": "Go standard web app",
            "description": "My personal website 🤗",
            "git": "https://gitlab.com/mathisf/mathisfaiv.re",
            "link": "https://mathisfaiv.re"
          }
        ]
      },
      "personal_interests": {
        "title": "Personal interests",
        "music": {
          "title": "Music",
          "list": [
            "Psychedelic rock",
            "classic rock",
            "alt rock",
            "electronica",
            "90's hip-hop/rap",
            "techno",
            "Blade's remixes 🩸"
          ]
        },
        "cinema": {
          "title": "Cinema",
          "list": [
            "David Lynch",
            "Quentin Dupieux",
            "Wes Anderson",
            "Sergio Leone",
            "A24 produced movies 👻"
          ]
        },
        "literature": {
          "title": "Literature",
          "list": ["Jack London"]
        },
        "video_games": {
          "title": "Video games",
          "list": [
            "Valve games",
            "Metroid",
            "Castlevania",
            "Shovel Knight",
            "The Binding of Isaac",
            "Earthbound/Mother 3"
          ]
        },
        "homelab": {
          "title": "Homelab",
          "list": [
            "Self-hosting my personal projects",
            "docker",
            "HomeAssistant",
            "Immich",
            "Grafana/Prometheus",
            "NAS",
            "borg"
          ]
        }
      }
    },
    "contact": {
      "title": "Contact",
      "links": "Social links",
      "email_form": {
        "title": "Send me an e-mail",
        "name": {
          "label": "Your name",
          "placeholder": "Name"
        },
        "email": {
          "label": "Your e-mail address",
          "placeholder": "you@domain.tld"
        },
        "message": {
          "label": "Your name",
          "placeholder": "Message"
        },
        "submit": "Send"
      },
      "email_button": "E-mail address",
      "gitlab_button": "Gitlab",
      "linkedin_button": "LinkedIn"
    }
  },
  "footer": {
    "back_to_top_button": "Back to top",
    "made": "Made with Go, <a href=\"https://picturepan2.github.io/spectre/\">Spectre.css</a>, <a href=\"https://rsms.me/inter/\">Inter</a> font and <a href=\"https://gotenberg.dev\">Gotenberg</a> for PDF generation",
    "copyrights": "&copy; 2024 Mathis F.",
    "licence": "(<a href=\"https://www.apache.org/licenses/LICENSE-2.0.txt\">Apache 2.0 Licence</a>)"
  }
}
