Version := $(shell git describe --tags --dirty)
# Version := "dev"
GitCommit := $(shell git rev-parse HEAD)
LDFLAGS := "-s -w -X main.Version=$(Version) -X main.GitCommit=$(GitCommit)"

.DEFAULT_GOAL := help

.PHONY: up
.PHONY: help
.PHONY: run
.PHONY: init-air
.PHONY: up
.PHONY: lint-fix
.PHONY: fix
.PHONY: lint
.PHONY: build-binaries
.PHONY: deploy

help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[0-9a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-30s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

run:
	go run cmd/main.go

init-air:
	go install github.com/air-verse/air@latest

air: init-air
	air --build.cmd "go build -o bin/mathisfaiv.re cmd/main.go" --build.bin "./bin/mathisfaiv.re"

up:
	COMPOSE_PROJECT_NAME=mathisfaiv_re docker compose -f ./deploy/compose.yml up --build --watch

docker-build:
	COMPOSE_PROJECT_NAME=mathisfaiv_re docker compose -f ./deploy/compose.yml build

lint-fix:
	go fmt ./...

fix: lint-fix

build-binaries:
	mkdir -p bin/
	CGO_ENABLED=0 GOOS=linux go build -mod=vendor -ldflags $(LDFLAGS) -o bin/mathisfaiv.re-amd64 ./cmd/main.go
	CGO_ENABLED=0 GOOS=darwin go build -mod=vendor -ldflags $(LDFLAGS)  -o bin/mathisfaiv.re-darwin ./cmd/main.go 
	GOARM=7 GOARCH=arm CGO_ENABLED=0 GOOS=linux go build -mod=vendor -ldflags $(LDFLAGS)  -o bin/mathisfaiv.re-arm  ./cmd/main.go  
	GOARCH=arm64 CGO_ENABLED=0 GOOS=linux go build -mod=vendor -ldflags $(LDFLAGS)  -o bin/mathisfaiv.re-arm64  ./cmd/main.go  
	GOOS=windows CGO_ENABLED=0 go build -mod=vendor -ldflags $(LDFLAGS)  -o bin/mathisfaiv.re.exe  ./cmd/main.go  

deploy:
	git pull
	COMPOSE_PROJECT_NAME=mathisfaiv_re docker compose -f deploy/compose.prod.yml build --no-cache
	COMPOSE_PROJECT_NAME=mathisfaiv_re docker compose -f deploy/compose.prod.yml down --remove-orphans
	COMPOSE_PROJECT_NAME=mathisfaiv_re docker compose -f deploy/compose.prod.yml up -d --force-recreate

